package com.putevodka.mvpex3.model

import com.putevodka.mvpex3.contract.ContractInterface

class MainActivityModel: ContractInterface.Model {

    private var myCounter = 0

    override fun getCounter()= myCounter

    override fun incrementCounter() {
        myCounter++
    }

    override fun resetCounter() {
        myCounter = 0
    }

    override fun decrementCounter() {
        if (myCounter >0) myCounter--
    }
}