package com.putevodka.mvpex3

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.putevodka.mvpex3.contract.ContractInterface
import com.putevodka.mvpex3.databinding.ActivityMainBinding
import com.putevodka.mvpex3.presenter.MainActivityPresenter
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), ContractInterface.View {

    private var presenter: MainActivityPresenter? = null
    private var binding: ActivityMainBinding? = null
    var empl = Employee(0, "Foolly Kislitsin", 0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d(javaClass.simpleName, "onCreate...")

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        attachPresenter()
    }

    override fun initView() {
        Log.d(javaClass.simpleName, "initView")

        Log.d(javaClass.simpleName, empl.lastName)

        binding?.setEmployee(empl)
        add_button.setOnClickListener { presenter?.incrementValue() }
        reset_button.setOnClickListener { presenter?.resetValue() }
        delete_button.setOnClickListener { presenter?.decrementValue() }
    }

    //   -----------
    private fun attachPresenter() {
        presenter = lastCustomNonConfigurationInstance as MainActivityPresenter?
        if (presenter == null) {
            presenter = MainActivityPresenter(this)
            Log.d(javaClass.simpleName, "presenter - null")
        }


        presenter?.attachView(this)
        add_button.setOnClickListener { presenter?.incrementValue() }
        reset_button.setOnClickListener { presenter?.resetValue() }
        delete_button.setOnClickListener { presenter?.decrementValue() }

    }

    override fun onDestroy() {
        presenter?.detachView()
        super.onDestroy()
    }

    override fun onRetainCustomNonConfigurationInstance(): Any {
        Log.d(javaClass.simpleName, "onRetain...")
        return presenter!!
    }

//------------------
    override fun uodateViewData() {
        val par = presenter?.getCounter()
        var inpar = 0
        if(par != null)  { inpar = par.toInt()}

        empl.numLike = inpar
        binding?.setEmployee(empl)
    }


}
