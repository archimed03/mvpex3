package com.putevodka.mvpex3.presenter

import com.putevodka.mvpex3.contract.ContractInterface
import com.putevodka.mvpex3.model.MainActivityModel

class MainActivityPresenter(_view: ContractInterface.View): ContractInterface.Presenter {

    private var view: ContractInterface.View? = _view
    private var model: ContractInterface.Model = MainActivityModel()

    init {
        view?.initView()
    }

    override fun incrementValue() {
        model.incrementCounter()
        view?.uodateViewData()
    }

    override fun resetValue() {
        model.resetCounter()
        view?.uodateViewData()
    }

    override fun decrementValue() {
        model.decrementCounter()
        view?.uodateViewData()
    }

    override fun getCounter() = model.getCounter().toString()

    override fun attachView(_view: ContractInterface.View) {
        this.view = _view
        view?.uodateViewData()
    }

    override fun detachView() {
        this.view = null
    }
}