package com.putevodka.mvpex3.contract

interface ContractInterface {

    interface View {
        fun initView()
        fun uodateViewData()
    }

    interface Presenter {
        fun incrementValue()
        fun decrementValue()
        fun resetValue()
        fun getCounter(): String
        fun attachView(V: View)
        fun detachView()
    }

    interface Model {
        fun getCounter(): Int
        fun incrementCounter()
        fun decrementCounter()
        fun resetCounter()
    }
}